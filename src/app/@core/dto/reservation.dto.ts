export class CreateReservationDto {
  name: string;
  email: string;
  phone: string;
  persons: number;
  date: Date;
  time: string;
  message: string;
}

export class ReservationRO {
  id: number;
  name: string;
  email: string;
  phone: string;
  persons: number;
  date: Date;
  time: string;
  message: string;
}
