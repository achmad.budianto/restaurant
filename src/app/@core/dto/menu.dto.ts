export class MenuRO {
  id: number;
  description: string;
  image: string;
  price: number;
  title: string;
}
