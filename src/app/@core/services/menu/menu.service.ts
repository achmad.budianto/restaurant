import { Injectable } from '@angular/core';
import { Configuration } from '../../config/configuration';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MenuRO } from '../../dto/menu.dto';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(
    private configuration: Configuration,
    private http: HttpClient,
  ) { }

  getMenus() {
    return this.http.get(this.configuration.apiURL + '/menus')
      .pipe(map(resp => resp as MenuRO[]));
  }
}
