import { Injectable } from '@angular/core';
import { Configuration } from '../../config/configuration';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { CreateReservationDto, ReservationRO } from '../../dto/reservation.dto';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(
    private configuration: Configuration,
    private http: HttpClient,
  ) { }

  createReservation(data: CreateReservationDto) {
    return this.http.post(this.configuration.apiURL + '/reservations', data)
      .pipe(map(resp => resp as ReservationRO));
  }
}
