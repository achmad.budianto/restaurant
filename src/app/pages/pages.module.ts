import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';
import { ThemeModule } from '../@theme/theme.module';
import { MenuComponent } from './menu/menu.component';


@NgModule({
  declarations: [HomeComponent, PagesComponent, MenuComponent],
  imports: [
    CommonModule,
    ThemeModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
