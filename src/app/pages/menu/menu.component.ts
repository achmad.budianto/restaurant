import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/@core/services/menu/menu.service';
import { MenuRO } from 'src/app/@core/dto/menu.dto';
import { Configuration } from 'src/app/@core/config/configuration';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menus: MenuRO[];
  apiUrl: string;

  constructor(
    private menuService: MenuService,
    private configuration: Configuration
  ) {
    this.apiUrl = configuration.apiURL;
  }

  ngOnInit(): void {
    this.menuService.getMenus().subscribe(response => {
      this.menus = response;
      console.log('this.menus: ', this.menus);
    });
  }

}
