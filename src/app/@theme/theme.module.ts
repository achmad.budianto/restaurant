import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { MainLayoutComponent } from './layouts';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

library.add(fas, far, fab);

const MODULES = [
  FontAwesomeModule,
  FormsModule,
  ReactiveFormsModule,
  NgbModule
];
const COMPONENTS = [
  NavbarComponent,
  FooterComponent,
  MainLayoutComponent,
];
const PIPES = [];

@NgModule({
  imports: [CommonModule, RouterModule, ...MODULES],
  exports: [CommonModule, ...PIPES, ...COMPONENTS],
  declarations: [...COMPONENTS, ...PIPES],
})
export class ThemeModule { }
