import { Component } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  styleUrls: ['./main.layout.scss'],
  template: `
    <app-navbar></app-navbar>
      <ng-content select="router-outlet"></ng-content>
    <app-footer></app-footer>
  `,
})
export class MainLayoutComponent {}
