import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReservationService } from 'src/app/@core/services/reservation/reservation.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  closeResult = '';
  ngbDate: NgbDateStruct;
  formGroup: FormGroup;

  constructor(
    private modalService: NgbModal,
    private reservationService: ReservationService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.createFormControl();
  }

  createFormControl() {
    this.formGroup = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      persons: ['', [Validators.required]],
      date: ['', []],
      time: ['', []],
      message: ['', []],
    });
  }

  open(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  submit() {
    this.formGroup.patchValue({
      date: new Date(this.ngbDate.year, this.ngbDate.month - 1, this.ngbDate.day)
    });
    this.reservationService.createReservation(this.formGroup.value).subscribe(response => {
      this.modalService.dismissAll();
      Swal.fire(
        'Success',
        'Your reservation will be proccessed',
        'success'
      );
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
